import React,{Component} from 'react';
import {createStackNavigator,createAppContainer} from 'react-navigation';

import home from './telas/Home';
import portalWeb from './telas/PortalDoAluno';
import presenca from './telas/RegistroPresenca';
import loginPresenca from './telas/LoginPresenca';
import biblioteca from './telas/Biblioteca';
import avisos from './telas/Avisos';

const HomeNavigatorStack = createStackNavigator({
    loginPresenca:{screen: loginPresenca},
    home:{screen: home},
    portalWeb:{screen: portalWeb,
        navigationOptions: {
            headerTitle:"Voltar",
        }},
    presenca:{screen: presenca,
        navigationOptions:{
            headerTitle:"Voltar"
        }},
    biblioteca:{screen: biblioteca,
        navigationOptions:{
            headerTitle:"Voltar"
        }},
    avisos: {screen: avisos,
    navigationOptions:{
        headerTitle:"Voltar"
    }}
},{
    initialRouteName:'loginPresenca',
    navigationOptions:{
        header: null,
    }
});


const AppNavigator = createStackNavigator({
    HomeNavigatorStack,
})

export default createAppContainer(AppNavigator)


