import React,{Component} from 'react';
import { StyleSheet, View, WebView } from 'react-native';


export default class portalDoAluno extends Component{
    render(){
        return(
            <WebView 
            style={styles.container} 
            source={{uri: 'http://meuportaldev.cruzeirodosul.edu.br/'}} 
            javaScriptEnabled={true}
            domStorageEnabled={true}
            startInLoadingState={true}
            thirdPartyCookiesEnabled={true}
            />
        )
    }
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        paddingTop: 15,
        backgroundColor: '#fff',
    },
});
