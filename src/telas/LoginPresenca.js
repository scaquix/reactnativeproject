import React,{Component} from 'react';
import {View, Text, TextInput,KeyboardAvoidingView,
    Button, ImageBackground,TouchableHighlight} from 'react-native';

import Styles from "../styles/loginPresencaStyle";

export default class LoginPresenca extends Component{
    static navigationOptions = {
        header: null,
      };

    constructor(props) {
    super(props);
    this.state = {  
        login: '',
        senha: '',
    };
}
    render(){
        return(
        <ImageBackground
            style={Styles.imageStyle}
            source={{uri: 'http://meuportaldev.cruzeirodosul.edu.br:30903/static//banners/banners-login/diversos/mobile/iStock-504650257-min.jpg'}} >
            <View style={Styles.overlay}>
             
            <KeyboardAvoidingView behavior="padding" enabled>
                <View>
                    
                    <TextInput style = {Styles.input}
                        onChangeText={(login) => this.setState({login})}
                        value={this.state.login} 
                        placeholder='Entre com seu RGM ou CPF'
                        placeholderTextColor='#ABA4A3'/>
                    
                    <TextInput style = {Styles.input}
                        onChangeText={(senha) => this.setState({senha})}
                        value={this.state.senha}
                        placeholder='Digite sua senha'
                        placeholderTextColor='#ABA4A3'/>
                    
                    <View style={Styles.optionsView}>
                        <View style={Styles.checkView}>
                            <TouchableHighlight style={Styles.touchStyle} onPress={() => this.props.navigation.navigate('home')}>
                                <Text style={Styles.textBtn}> ACESSE  </Text>
                            </TouchableHighlight>
                        </View>
                    </View>
                    
                </View>   
            </KeyboardAvoidingView>
            </View>     
        </ImageBackground>
                
        )
    }
}

