import React,{Component} from 'react';
import { 
  TouchableHighlight
  ,Linking, Platform, 
  StyleSheet, Text,
  ScrollView, 
  View, Button, ActivityIndicator, ImageBackground,Image} from 'react-native';
import AppLink from 'react-native-app-link';

import Icon from 'react-native-vector-icons/FontAwesome';
import Ionicicons from 'react-native-vector-icons/Entypo';



import Styles from '../styles/homeStyle';

export const myConst = {
  blackboardApp: {
    ios: {
      storeUrl: 'itms-apps://itunes.apple.com/nl/app/blackboard-mobile-learn/id376413870?mt=8',
      appId: 'bblearn://'
    },
    android: {
      storeUrl: 'market://details?id=com.blackboard.android.bbstudent',
      appId: 'com.blackboard.android.bbstudent'
    }
  }
}

export default class Home extends Component {
  static navigationOptions = {
    header: null,
  };
    render() {
  
        return (
          <View style={Styles.mainView}>
              <ImageBackground
              style={Styles.imageStyle}
              source={{uri: 'http://meuportaldev.cruzeirodosul.edu.br:30903/static//banners/banners-login/diversos/mobile/iStock-504650257-min.jpg'}} >
              <ScrollView >
                <View style={Styles.overlay}>
                <View style={Styles.viewPedding}>
                <View style={Styles.viewLogo}>
                <Image style={Styles.logoStyle}
                source={{uri: 'http://meuportaldev.cruzeirodosul.edu.br:30903/static//logos/1/logo-negativo_1.png'}} />
                </View>
                </View>
              <View style={Styles.viewPedding}>
                <View style={Styles.checkView}>
                  <TouchableHighlight style={Styles.touchStyle} onPress={() => this.props.navigation.navigate('presenca')}>
                  <View  style={Styles.iconCenter} >
                  <Text> <Icon name="check" size={40} /> </Text>
                  <Text style={Styles.styleFonte}> Check-in </Text>
                  </View>
                  </TouchableHighlight>
                </View>
              </View>

              <View style={Styles.optionsView}>
              
                <View style={Styles.botoesView}>
                  <TouchableHighlight style={Styles.touchStyle} onPress={() => this.props.navigation.navigate('portalWeb')}>
                  <View  style={Styles.iconCenter} >
                  <Text> <Icon name="graduation-cap" size={40} /> </Text>
                  <Text style={Styles.styleFonte}> Aluno </Text>
                  </View>
                  </TouchableHighlight>
                </View>

                <View style={Styles.botoesView}>
                  <TouchableHighlight style={Styles.touchStyle} onPress={() => this.props.navigation.navigate('biblioteca')}>
                  <View  style={Styles.iconCenter} >
                  <Text> <Icon name="book" size={40} /> </Text>
                  <Text style={Styles.styleFonte}> Biblioteca </Text>
                  </View>
                  </TouchableHighlight>
                </View>

                <View style={Styles.botoesView}>
                  <TouchableHighlight  style={Styles.touchStyle} onPress={this.openBB}>
                  <View  style={Styles.iconCenter} >
                  <Text> <Ionicicons name="blackboard" size={40} /> </Text>
                  <Text style={Styles.styleFonte}> Blackboard </Text>
                  </View>
                  </TouchableHighlight>
                </View>

                <View style={Styles.botoesView}>
                  <TouchableHighlight  style={Styles.touchStyle} onPress={() => this.props.navigation.navigate('avisos')}>
                  <View  style={Styles.iconCenter} >
                  <Text> <Icon name="envelope" size={40} /> </Text>
                  <Text style={Styles.styleFonte}> Avisos </Text>
                  </View>
                  </TouchableHighlight>
                </View>
              </View>
            </View>
          </ScrollView>
          </ImageBackground>
          </View>
        );
      }
     
    // Abrir BlackBoard
    openBB() { 
      AppLink.maybeOpenURL('bbstudent://', { 
        appName:'Blackboard',  
        appStoreId:null, 
        appStoreLocale:'pt_BR', 
        playStoreId:'com.blackboard.android.bbstudent'
 
       }).then((retorno) => {
        console.log('retorno', retorno);
        
      })
      .catch((err) => {
        console.log('err', err);
      });

    }
  }



