import React,{Component} from 'react';
import { StyleSheet, View, WebView } from 'react-native';

export default class BibliotecaWebView extends Component{
    render(){
        return(
            <WebView 
            style={styles.container} 
            source={{uri: 'http://biblioteca.cruzeirodosul.edu.br/pergamum_cruzeirodosul/mobile/index.php'}} 
            javaScriptEnabled={true}
            domStorageEnabled={true}
            startInLoadingState={true}
            thirdPartyCookiesEnabled={true}
            />
        );
    }
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        paddingTop: 15,
        backgroundColor: '#fff',
    },
});