import React,{Component} from 'react';
import { StyleSheet, Text, View, Button, ActivityIndicator} from 'react-native';

import Styles from '../styles/presencaStyle'
import Icon from 'react-native-vector-icons/Ionicons';
import Moment from 'moment';

export default class RegistroPresenca extends Component{

tempo = null;

constructor(props) {
    super(props);
    this.state = {  
        curTime: null,
    };
}

componentDidMount() {
    this.tempo = setInterval( () => {
      this.setState({
        curTime: Moment(new Date()).format("DD/MM/YYYY  h:mm:ss")
      })
    },1000)
  }
componentWillUnmount(){
    clearInterval(this.tempo)
}

enviaPresenca(){
    fetch("https://gatewaydev.cruzeirodosul.edu.br:31090/cse-acd-estagio/webestagio/log/inserir", {
        method: 'POST',
        headers: new Headers({
            'Content-Type': 'application/json', 
          }),
          body:JSON.stringify({
            "codNiv": "3",
            "idAtv": "1234567",
            "grupo": "2",
            "obsAtv":"react-native",
            "serie": "2",
            "tpParam": "ALUNO",
            "horFin": "120",
            "horIni": "100",
        })
      })
      .then((response) => response.text())
      .then((responseText) => {
        // alert(responseText);
        alert("Presença Registrada com sucesso !")
      })
      .catch((error) => {
          console.error(error);
      });
}

    render(){    
        
        return(
            <View style={Styles.mainView}>
                <View style={Styles.timerStyle}>
                    <Text style={Styles.textStyle}> 000 - Nome da disciplina  </Text>
                    <Text style={Styles.timerFont}> {this.state.curTime} </Text>
                </View>
                <View style={Styles.iconCenter}>
                    <Text> <Icon name="ios-finger-print" size={220} color={'#003168'} onPress={() => { this.enviaPresenca()}} /> </Text>
                </View>
            </View>
        )
    
}
}
