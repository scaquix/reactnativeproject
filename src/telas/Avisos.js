import React, { Component } from "react";
import { View, Text, StyleSheet,FlatList,Alert,TouchableHighlight} from 'react-native';

import Swipeout from 'react-native-swipeout';
import flatListData from '../flatlistTeste';


class FlatListItem extends Component{
  constructor(props){
    super(props);
    this.state={
      activeRowKey: null
    };
  }
  render(){
    const swipeSettings ={
      autoClose: true,
      onClose: (secId,rowId,direction) =>{
        if(this.state.activeRowKey != null){
          this.setState({activeRowKey: null});
        }
      },
      onOpen: (secId,rowId,direction) =>{
        this.setState({activeRowKey: this.props.item.key});
      },
      right: [
        {
          onPress: () => {
              const deletingRow = this.state.activeRowKey;
              Alert.alert(
                'Atenção!',
                'Tem certeza que deseja apagar esta notificação?',
                [
                  {text: 'Não', onPress: () => null},
                  {text: 'Sim', onPress: () => {
                    flatListData.splice(this.props.index, 1);
                    this.props.parentFlatList.atualizaListaDel(deletingRow); 
                  }},
                ],
                {cancelable: true}
              );
          },
          text: 'Apagar', type: 'delete'
        }
      ],
      rowId: this.props.index,
      sectionId: 1
    };


    return(
      <Swipeout {...swipeSettings}>
      <View style= {{
        flex: 1,
        flexDirection: 'row'
      }}>
        <TouchableHighlight onPress={() => Alert.alert('funcionando click', 'click msg') }
        style= {{
          flex: 1,
          flexDirection: 'row',
          backgroundColor: this.props.index % 2 == 0 ? '#F5F5F5' : 'white'
        }}>

          <View style={{
            flex: 1,
            flexDirection: 'column',
            height: 60
          }}>
            <Text style={{ fontSize: 14, color: 'black'}}> {this.props.item.titulo} </Text>
            <Text style={{ fontSize: 14, color: 'black'}}> {this.props.item.descricaoResu} </Text>
          </View>

        </TouchableHighlight>
      </View>
      </Swipeout>
    );
  }
}

export default class Avisos extends Component{
  constructor(props){
    super(props);
    this.state = ({
      deletedRowKey: null,    
    });
  }
  atualizaListaDel = (deletedKey) => {
    this.setState((prevState) => {
      return {
        deletedRowKey: deletedKey
      }
    });
  }
  render(){
    return(
      <View style={{flex: 1, marginTop: 34}}>
        <FlatList
        data={flatListData}
        renderItem={({item,index}) => {
          return(
            <FlatListItem item={item} index={index} parentFlatList={this}/>
          );
        }}>

        </FlatList>
      </View>
    );
  }
}