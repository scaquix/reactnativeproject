var flatListData = [
    {
        "key": "1",
        "titulo":"Teste título",
        "descricaoResu": "teste de aviso",
        "descricaoDet": "testando lista para quadro de avisos"
    },
    {
        "key": "2",
        "titulo":"Segundo teste",
        "descricaoResu": "Segundo teste",
        "descricaoDet": "testando lista pela segunda vez"
    },
    {
        "key": "3",
        "titulo":"3 teste",
        "descricaoResu": "Terceiro teste",
        "descricaoDet": "testando titulo com numero"
    },
    {
        "key": "4",
        "titulo":"123456789",
        "descricaoResu": "Quarto teste",
        "descricaoDet": "Testando título somente com numeros"
    },
    {
        "key": "5",
        "titulo":"ultimo",
        "descricaoResu": "Quinto teste",
        "descricaoDet": "fim da linha"
    },
];

module.exports = flatListData;