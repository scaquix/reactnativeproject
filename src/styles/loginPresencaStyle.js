import React from 'react'

import {StyleSheet} from 'react-native';

export default Styles = StyleSheet.create({
    input: {
        margin: 15,
        height: 40,
        borderColor: 'white',
        borderWidth: 0,
        borderBottomWidth: 1,
        fontSize: 14,
        color: 'white'
     },
     imageStyle:{
        flex: 1,
        flexDirection: 'column', 
        justifyContent: 'center',
     },
     checkView:{
        borderRadius: 8, 
        alignItems: 'center', 
        justifyContent: 'center', 
        width: '100%',
        height: 50, 
        backgroundColor: '#e4ab00b3',
    },
     overlay: {
        flex: 1, 
        flexDirection: 'column', 
        justifyContent: 'center', 
        alignItems: 'stretch',
        backgroundColor: '#00000099',
    },
    textLogin:{
        fontSize: 22,
        color: 'white',
        marginBottom: 30,
    },
    viewTextLogin:{
        justifyContent: 'center',
        alignItems: 'center'
    },
    optionsView:{
        padding:30,
    },
    textBtn:{
        color: 'white',
        fontSize: 16,
        fontWeight: 'bold',
    },
    touchStyle:{
        width:'100%',
        height:'100%',
        alignItems: 'center', 
        justifyContent: 'center',
    },
 })