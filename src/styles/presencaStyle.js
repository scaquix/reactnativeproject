import React from 'react';

import {StyleSheet} from 'react-native';


export default Styles = StyleSheet.create({
    iconCenter:{
        flex:1,
        alignItems:'center',
        justifyContent:'center',
        backgroundColor:'#e8e8e8', 

    },
    mainView:{
        flex: 1, 
        flexDirection: 'column', 
        justifyContent: 'center', 
        alignItems: 'stretch',
        overflow: 'visible',
    },
    timerStyle:{
        alignItems:'center',
        justifyContent:'center',
        fontWeight: 'bold',
        borderBottomWidth: 2,
        borderBottomColor: '#003168',
        minHeight: 150, 
        padding: 8
        
    },
    timerFont:{
        fontSize: 25,
        fontWeight: 'bold',
    },
    textStyle:{
        textAlign:'center',
        fontSize: 16
    },

})