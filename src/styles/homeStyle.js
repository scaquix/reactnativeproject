import React from 'react';

import {StyleSheet} from 'react-native';



export default Styles = StyleSheet.create({
    mainView:{
        flex: 1, 
        flexDirection: 'column', 
        justifyContent: 'center', 
        alignItems: 'stretch',
        overflow: 'visible'
    },
    imageStyle:{
        flex: 1,
        flexDirection: 'column', 
        justifyContent: 'center', 
    },
    checkView:{
        borderRadius: 8, 
        alignItems: 'center', 
        justifyContent: 'center', 
        margin: '1%',
        width: '98%',
        height: 100, 
        backgroundColor: '#ffffff80',
        
    },
    optionsView:{
        flexDirection: 'row',
        flexWrap: 'wrap',
        paddingLeft: 8,
        paddingRight: 8,
        
    },
    botoesView:{
        borderRadius: 8, 
        alignItems: 'center', 
        justifyContent: 'center', 
        margin: '1%', 
        width: '48%', 
        height: 150, 
        backgroundColor: '#ffffff80' 
    },
    touchStyle:{
        width:'100%',
        height:'100%',
        alignItems: 'center', 
        justifyContent: 'center', 
    },
    iconCenter:{
        borderRadius: 8, 
        alignItems: 'center', 
        justifyContent: 'center', 
        margin: '1%',
        width: '98%',
        height: 100, 
    },
    styleFonte:{
        fontSize: 16,
    },
    viewPedding:{
        paddingLeft: 8,
        paddingRight: 8

    },teste:{
        backgroundColor:"red"
    },
    logoStyle:{
        width: 250, 
        height: 60   
    },
    viewLogo:{
        borderRadius: 8, 
        alignItems: 'center', 
        justifyContent: 'flex-start', 
        margin: '1%',
        width: '98%',
        height: 100,
        marginBottom: 15  
    },
    overlay: {
        flex: 1, 
        flexDirection: 'column', 
        justifyContent: 'center', 
        alignItems: 'stretch',
        backgroundColor: '#00000099',
    },

});
