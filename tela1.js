import React,{Component} from 'react'
import { StyleSheet, Text, View, Button, ActivityIndicator} from 'react-native';


class tela1 extends Component {
    constructor(props){
      super(props);
      this.state = {
        isLoading: true,
        dataSource: null,
      }
    }
    componentDidMount(){
      return fetch('https://gatewaydev.cruzeirodosul.edu.br:31090/cse-acd-parametrizacao/parametrizacao/academico/12')
        .then ((response) => response.json())
        .then((responseJson) => {
            this.setState({
              isLoading: false,
              dataSource: responseJson,
          })
        })
        .catch((error) => {
          console.log(error)
        });
    
      }
    render() {
      if(this.state.isLoading){
        return (
          <View> 
            <ActivityIndicator/>
          </View>
        )
      }else{
        let movies = this.state.dataSource.map((val, key) => {
          return <View key={key}>
                  <Text>{val.idParacd} testando {val.anoLeti} </Text>
                  <Text></Text>
                 </View>
        });
        return (
          <View>
            {movies}
          </View>
          
      );
    }
  }
  }

  export default tela1;